import java.util.Calendar;
import java.util.Set;

public class CarInfo implements compare{

    private Set<String> brand;
    protected float price;
    private int mileage;
    private int yearOfProduction;
    private Set<String> region;
    private Set<String> city;
    private boolean damaged;
    private boolean isNew;
    private String date;

    public float getPrice () {
        return price;
    }
    public int getYear () {
        return yearOfProduction;
    }
    public int getMileage () {
        return mileage;
    }
    public String getDate() {setDate();return date;
    }

    public void setDate() {
        Calendar time = Calendar.getInstance();
        int day = time.get(Calendar.DATE);
        date = Integer.toString(day);
    }

    public boolean isNew() {
        return isNew;
    }

    public boolean isDamaged() {
        return damaged;
    }

    public Set<String> getBrand() {
        return brand;
    }

    public Set<String> getRegion() {
        return region;
    }

    public Set<String> getCity() {
        return city;
    }

}

interface compare {
    float getPrice();
    int getYear();
    int getMileage();
    String getDate();
}


