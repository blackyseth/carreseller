import java.util.*;

public class CarsComparator{

    public enum sortBy {price,mileage,yearofproduction,date}

    public static void main(String[] args) {

        List<CarInfo> carInfoList = new ArrayList<>();

        sortBy order = sortBy.price;

        switch (order){
            case price: carInfoList.sort(Comparator.comparing(CarInfo::getPrice));
            case mileage: carInfoList.sort(Comparator.comparing(CarInfo::getMileage));
            case date: carInfoList.sort(Comparator.comparing(CarInfo::getDate));
            case yearofproduction: carInfoList.sort(Comparator.comparing(CarInfo::getYear));
        }

     }
}


